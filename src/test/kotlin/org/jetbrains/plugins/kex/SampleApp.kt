package org.jetbrains.plugins.kex

import org.jetbrains.plugins.kex.core.HexViewSettings
import org.jetbrains.plugins.kex.core.data.FakeDataSource
import org.jetbrains.plugins.kex.core.data.FileDataSource
import org.jetbrains.plugins.kex.core.ui.HexViewTableModel
import java.awt.Menu
import java.awt.MenuBar
import java.awt.MenuItem
import javax.swing.JFileChooser
import javax.swing.JFrame
import javax.swing.JTable
import javax.swing.SwingUtilities

@Suppress("UndesirableClassUsage")
class SampleApp {

    private val settings = HexViewSettings(
        columns = 10,
        wordLength = 2
    )

    private val myFrame = createAndShowGUI()

    private fun openFile() {
        val dialog = JFileChooser()
        dialog.isMultiSelectionEnabled = false
        when (dialog.showDialog(myFrame, "Select")) {
            JFileChooser.APPROVE_OPTION -> {
                myFrame.contentPane.removeAll()
                myFrame.contentPane.add(
                    JTable(
                        HexViewTableModel(
                            settings,
                            FileDataSource(dialog.selectedFile.toPath()),
                            0
                        )
                    )
                )
                myFrame.isVisible = true
            }
            else -> {
            }
        }
    }

    private fun createAndShowGUI(): JFrame {
        val frame = JFrame("TestKexApp")
        frame.defaultCloseOperation = JFrame.EXIT_ON_CLOSE

        val table = JTable(
            HexViewTableModel(
                settings,
                FakeDataSource(),
                0
            )
        )
        frame.contentPane.add(table)
        frame.menuBar = MenuBar()
        val menu = Menu("File")
        val item = MenuItem("Open")
        item.addActionListener { openFile() }
        menu.add(item)
        frame.menuBar.add(menu)

        //Display the window.
        frame.pack()
        frame.isVisible = true
        return frame
    }

}

fun main(args: Array<String>) {
    SwingUtilities.invokeLater { SampleApp() }
}