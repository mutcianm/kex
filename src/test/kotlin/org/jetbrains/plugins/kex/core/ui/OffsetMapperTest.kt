package org.jetbrains.plugins.kex.core.ui

import org.jetbrains.plugins.kex.core.DefaultHexVewSettings
import org.junit.Test
import org.junit.Assert.*

class OffsetMapperTestSingleByteWord {

    @Test
    fun testSimpleNoAddrNoASCII() {
        val mapper = DefaultOffsetMapper(
            DefaultHexVewSettings.copy(
                wordLength = 1,
                showAddress = false,
                showAscii = false,
                columns = 4
            )
        )
        assertEquals(TableElementKind.WORD, mapper.getKind(0,0))
        assertEquals(0, mapper.xyToOffset(0,0,0))
        assertEquals(3, mapper.xyToOffset(0,3,0))
        assertEquals(4, mapper.xyToOffset(1,0,0))
        assertEquals(7, mapper.xyToOffset(1,3,0))
    }

    @Test
    fun testWithAddrNoASCII() {
        val mapper = DefaultOffsetMapper(
            DefaultHexVewSettings.copy(
                wordLength = 1,
                showAddress = true,
                showAscii = false,
                columns = 4
            )
        )
        assertEquals(TableElementKind.ADDR, mapper.getKind(0,0))
        assertEquals(TableElementKind.ADDR, mapper.getKind(1,0))
        assertEquals(-1, mapper.xyToOffset(0,0,0))
        assertEquals(2, mapper.xyToOffset(0,3,0))
        assertEquals(2, mapper.xyToOffset(1,0,0))
        assertEquals(5, mapper.xyToOffset(1,3,0))
    }

    @Test
    fun testWithAddrWithASCII() {
        val mapper = DefaultOffsetMapper(
            DefaultHexVewSettings.copy(
                wordLength = 1,
                showAddress = true,
                showAscii = true,
                columns = 4
            )
        )
        assertEquals(TableElementKind.ADDR, mapper.getKind(0,0))
        assertEquals(TableElementKind.ASCII, mapper.getKind(0,3))
        assertEquals(TableElementKind.ADDR, mapper.getKind(1,0))
        assertEquals(TableElementKind.ASCII, mapper.getKind(1,3))
        assertEquals(TableElementKind.WORD, mapper.getKind(1,2))
        assertEquals(-1, mapper.xyToOffset(0,0,0))
        assertEquals(2, mapper.xyToOffset(0,3,0))
        assertEquals(1, mapper.xyToOffset(1,0,0))
        assertEquals(3, mapper.xyToOffset(1,2,0))
    }
}