package org.jetbrains.plugins.kex.core.ui

import org.jetbrains.plugins.kex.core.HexViewSettings

interface OffsetMapper {
    fun getKind(x: Int, y: Int): TableElementKind
    fun xyToOffset(x: Int, y: Int, loadAddr: Long): Int
}

class DefaultOffsetMapper(private val settings: HexViewSettings):
    OffsetMapper {

    private val lm = if (settings.showAddress) -1 else 0 // left margin eaten up by address column
    private val rm = if (settings.showAscii) -1 else 0   // right margin by ascii view
    private val nY = settings.columns

    override fun getKind(x: Int, y: Int): TableElementKind {
        return when {
            y == 0 && settings.showAddress                  -> TableElementKind.ADDR
            y == settings.columns-1 && settings.showAscii   -> TableElementKind.ASCII
            else                                            -> TableElementKind.WORD
        }
    }

    // x is row, y is col
    override fun xyToOffset(x: Int, y: Int, loadAddr: Long): Int {
        return loadAddr.toInt() +
                ((nY + lm) * x + y + lm + rm*x)*settings.wordLength
    }
}