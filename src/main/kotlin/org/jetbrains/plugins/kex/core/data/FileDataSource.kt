package org.jetbrains.plugins.kex.core.data

import com.intellij.util.io.exists
import com.intellij.util.io.isWritable
import com.intellij.util.io.size
import java.nio.channels.FileChannel
import java.nio.file.Path
import java.nio.file.StandardOpenOption

class FileDataSource(private val path: Path) : AbstractDataSource {
    override fun isValid(): Boolean {
        return path.exists()
    }

    override fun isReadOnly(): Boolean {
        return path.isWritable
    }

    override fun capacity(): Long {
        return path.size()
    }

    override fun fetchBuffer(address: Long): ByteBufferWithAddress {
        val buffer = FileChannel
            .open(path, StandardOpenOption.READ, StandardOpenOption.WRITE)
            .map(FileChannel.MapMode.READ_WRITE, address, path.size() - address)
        return ByteBufferWithAddress(buffer, address, path.size())
    }
}