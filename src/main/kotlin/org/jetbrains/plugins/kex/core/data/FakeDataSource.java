package org.jetbrains.plugins.kex.core.data;

import java.nio.ByteBuffer;

public class FakeDataSource implements AbstractDataSource {

    private byte[] data = new byte[1024];
    private ByteBufferWithAddress buffer = new ByteBufferWithAddress(ByteBuffer.wrap(data), 0, 127);

    public FakeDataSource() {
        for (int i = 0; i < data.length; i++) {
            data[i] = (byte) (i % Byte.MAX_VALUE);
        }
    }

    @Override
    public boolean isValid() {
        return true;
    }

    @Override
    public boolean isReadOnly() {
        return true;
    }

    @Override
    public long capacity() {
        return data.length;
    }

    @Override
    public ByteBufferWithAddress fetchBuffer(long address) {
        return buffer;
    }

}
