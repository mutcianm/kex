package org.jetbrains.plugins.kex.core.data;

public interface AbstractDataSource {
    boolean isValid();
    boolean isReadOnly();
    long capacity();
    ByteBufferWithAddress fetchBuffer(long address);
}
