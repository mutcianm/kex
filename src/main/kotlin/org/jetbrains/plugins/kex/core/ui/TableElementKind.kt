package org.jetbrains.plugins.kex.core.ui

enum class TableElementKind {
    ADDR, WORD, ASCII
}