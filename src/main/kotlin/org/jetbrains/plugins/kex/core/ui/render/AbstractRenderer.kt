package org.jetbrains.plugins.kex.core.ui.render

import org.jetbrains.plugins.kex.core.data.ByteBufferWithAddress

interface AbstractRenderer<P,S> {
    fun render(position: P, source: S): String
}

interface ByteBufferRenderer:
    AbstractRenderer<Int, ByteBufferWithAddress>