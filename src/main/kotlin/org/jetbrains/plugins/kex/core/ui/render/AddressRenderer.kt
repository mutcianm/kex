package org.jetbrains.plugins.kex.core.ui.render

import org.jetbrains.plugins.kex.core.HexViewSettings
import org.jetbrains.plugins.kex.core.data.ByteBufferWithAddress

class AddressRenderer(private val settings: HexViewSettings):
    ByteBufferRenderer {
    override fun render(position: Int, source: ByteBufferWithAddress): String {
        return "0x%08x".format(position + settings.wordLength)
    }
}