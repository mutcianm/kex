package org.jetbrains.plugins.kex.core.ui

import org.jetbrains.plugins.kex.core.HexViewSettings
import org.jetbrains.plugins.kex.core.data.AbstractDataSource
import org.jetbrains.plugins.kex.core.ui.render.ASCIIRenderer
import org.jetbrains.plugins.kex.core.ui.render.AddressRenderer
import org.jetbrains.plugins.kex.core.ui.render.WordRenderer
import org.jetbrains.plugins.kex.core.wordColumns
import javax.swing.table.AbstractTableModel
import kotlin.math.min

// x is row, y is col
class HexViewTableModel(private val settings: HexViewSettings, private val source: AbstractDataSource, private val loadAddr: Long = 0): AbstractTableModel() {

    data class RenderedASCII(val repr: String)
    data class Address(val repr: Long)

    private val wordRenderer = WordRenderer(settings)
    private val addressRenderer = AddressRenderer(settings)
    private val asciiRenderer = ASCIIRenderer(settings)
    private val offsetMapper = DefaultOffsetMapper(settings)

    override fun getRowCount(): Int {
        return min((source.capacity() / settings.wordColumns() / settings.wordLength).toInt() + 1, 128)
    }

    override fun getColumnCount(): Int = settings.columns

    override fun getValueAt(x: Int, y: Int): Any {
        return if (loadAddr > 0) { // prepend empty space space for dynamic scrolling
            "???"
        } else {
            val offset = offsetMapper.xyToOffset(x, y, loadAddr)
            val buffer = source.fetchBuffer(loadAddr)
            when (offsetMapper.getKind(x, y)) {
                TableElementKind.ADDR -> addressRenderer.render(offset, buffer)
                TableElementKind.WORD  -> wordRenderer.render(offset, buffer)
                TableElementKind.ASCII -> asciiRenderer.render(offset, buffer)
            }
        }
    }

    override fun getColumnClass(columnIndex: Int): Class<*> {
        return if (settings.showAscii && columnIndex >= settings.columns)
            RenderedASCII::class.java
          else if (settings.showAddress && columnIndex == 0)
            Address::class.java
          else
            String::class.java
    }
}