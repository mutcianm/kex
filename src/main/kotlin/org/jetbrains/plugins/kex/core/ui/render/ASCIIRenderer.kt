package org.jetbrains.plugins.kex.core.ui.render

import org.jetbrains.plugins.kex.core.HexViewSettings
import org.jetbrains.plugins.kex.core.data.ByteBufferWithAddress
import org.jetbrains.plugins.kex.core.wordColumns
import kotlin.math.min

class ASCIIRenderer(settings: HexViewSettings) : ByteBufferRenderer {

    private val buffer = ByteArray(settings.wordColumns() * settings.wordLength)

    override fun render(position: Int, source: ByteBufferWithAddress): String {
        return try {
            buffer.fill(0)
            source.repr.position(position-buffer.size)
            source.repr.get(buffer, 0, min(buffer.size, source.repr.capacity() - position + buffer.size))
            for (i in (0 until buffer.size)) {
                if (buffer[i] < 32)
                    buffer[i] = 46
            }
            String(buffer)
        } catch (e: Exception) {
            "????"
        }
    }
}