package org.jetbrains.plugins.kex.core.data

import java.nio.ByteBuffer

class ByteBufferWithAddress(val repr: ByteBuffer, val start: Long, val end: Long) {
    fun size() = end - start
}