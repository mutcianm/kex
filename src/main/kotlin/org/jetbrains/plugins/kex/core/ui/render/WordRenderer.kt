package org.jetbrains.plugins.kex.core.ui.render

import org.jetbrains.plugins.kex.core.HexViewSettings
import org.jetbrains.plugins.kex.core.data.ByteBufferWithAddress
import kotlin.math.min

class WordRenderer(settings: HexViewSettings) :
    ByteBufferRenderer {

    private val buffer = ByteArray(settings.wordLength)

    override fun render(position: Int, source: ByteBufferWithAddress): String {
        return try {
            buffer.fill(0)
            source.repr.position(position)
            source.repr.get(buffer, 0, min(buffer.size, source.repr.capacity() - position))
            buffer.joinToString(separator = " ") { "%02x".format(it) }
        } catch (e: Exception) {
            "END"
        }
    }
}