package org.jetbrains.plugins.kex.core

data class HexViewSettings(
    val columns: Int = 16,
    val wordLength: Int = 4,
    val radix: Int = 16,
    val byteOrder: KBYTE_ORDER = KBYTE_ORDER.LITTLE,
    val showAscii: Boolean = true,
    val showAddress: Boolean = true
)

fun Boolean.int() = if (this) 1 else 0

fun HexViewSettings.wordColumns() = columns - showAddress.int() - showAscii.int()

enum class KBYTE_ORDER { BIG, LITTLE }

val DefaultHexVewSettings = HexViewSettings()