package org.jetbrains.plugins.kex.fileEditor

import com.intellij.codeHighlighting.BackgroundEditorHighlighter
import com.intellij.openapi.fileEditor.*
import com.intellij.openapi.project.Project
import com.intellij.openapi.util.Key
import com.intellij.openapi.vfs.VirtualFile
import java.beans.PropertyChangeListener
import javax.swing.JComponent

class HexFileEditor(project: Project, file: VirtualFile) : FileEditor {
    override fun isModified(): Boolean = false

    override fun addPropertyChangeListener(listener: PropertyChangeListener) {
    }

    override fun getName(): String = "HexFileEditor"

    override fun setState(state: FileEditorState) {
    }

    override fun getComponent(): JComponent = MyHexEditorComponent()

    override fun getPreferredFocusedComponent(): JComponent? = null

    override fun <T : Any?> getUserData(key: Key<T>): T? = null

    override fun selectNotify() {
    }

    override fun <T : Any?> putUserData(key: Key<T>, value: T?) {
    }

    override fun getCurrentLocation(): FileEditorLocation? = null

    override fun deselectNotify() {
    }

    override fun getBackgroundHighlighter(): BackgroundEditorHighlighter? = null

    override fun isValid(): Boolean = true

    override fun removePropertyChangeListener(listener: PropertyChangeListener) {
    }

    override fun dispose() {
    }
}