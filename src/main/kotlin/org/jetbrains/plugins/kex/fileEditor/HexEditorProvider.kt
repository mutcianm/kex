package org.jetbrains.plugins.kex.fileEditor

import com.intellij.openapi.fileEditor.FileEditor
import com.intellij.openapi.fileEditor.FileEditorPolicy
import com.intellij.openapi.fileEditor.FileEditorProvider
import com.intellij.openapi.project.Project
import com.intellij.openapi.vfs.VirtualFile

class HexEditorProvider: FileEditorProvider {
    init {
        myInstance = this
    }

    private val filesToOpen = mutableSetOf<VirtualFile>()

    override fun getEditorTypeId(): String = "HexEditorProvider"

    override fun accept(project: Project, file: VirtualFile): Boolean = file in filesToOpen

    override fun createEditor(project: Project, file: VirtualFile): FileEditor {
        filesToOpen -= file
        return HexFileEditor(project, file)
    }

    override fun getPolicy(): FileEditorPolicy = FileEditorPolicy.PLACE_AFTER_DEFAULT_EDITOR

    fun acceptFile(file: VirtualFile) {
        filesToOpen += file
    }

    companion object {
        private var myInstance: HexEditorProvider? = null
        val INSTANCE: HexEditorProvider?
            get() = myInstance
    }
}