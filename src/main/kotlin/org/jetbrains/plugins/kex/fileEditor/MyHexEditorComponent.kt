package org.jetbrains.plugins.kex.fileEditor

import com.intellij.ui.components.JBLabel
import com.intellij.ui.table.JBTable
import org.jetbrains.plugins.kex.core.DefaultHexVewSettings
import org.jetbrains.plugins.kex.core.data.FakeDataSource
import org.jetbrains.plugins.kex.core.ui.HexViewTableModel
import javax.swing.JPanel

class MyHexEditorComponent: JPanel() {

    private val mainTable by lazy {
        JBTable(HexViewTableModel(DefaultHexVewSettings, FakeDataSource()))
    }

    init {
        add(mainTable)
        add(JBLabel("HELLOWORLD"))
    }
}