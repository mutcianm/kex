package org.jetbrains.plugins.kex

import com.intellij.openapi.actionSystem.AnAction
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.actionSystem.CommonDataKeys
import com.intellij.openapi.fileEditor.FileEditorManager
import org.jetbrains.plugins.kex.fileEditor.HexEditorProvider

class OpenInHexEditorAction: AnAction("Open in HexEditor") {
    override fun actionPerformed(e: AnActionEvent) {
        val files = e.getData(CommonDataKeys.VIRTUAL_FILE_ARRAY)
        files?.forEach { HexEditorProvider.INSTANCE?.acceptFile(it) }
        files?.forEach { FileEditorManager.getInstance(e.project!!).openFile(it, true, false) }
    }

    override fun update(e: AnActionEvent) {
        val files = e.getData(CommonDataKeys.VIRTUAL_FILE_ARRAY)
        if (files == null || files.size != 1 || files[0].isDirectory)
            e.presentation.isEnabled = false
    }
}